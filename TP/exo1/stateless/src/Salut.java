package stateless.src;
import javax.ejb.Remote;

@Remote
public interface Salut{
    public String salut(String slt);
}
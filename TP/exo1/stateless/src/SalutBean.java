package stateless.src;
import javax.ejb.Stateless;

@Stateless
public class SalutBean implements Salut {
    @Override
    public String salut(String slt){
        return "Salut "+slt;
    }
}

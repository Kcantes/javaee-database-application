package stateful.src;
import javax.ejb.Stateful;

@Stateful
public class ReSalutBean implements ReSalut {
    String message="";

    public String salut(String slt) {
        message = slt;
        return "Salut " + slt;
    }
    public String re_salut(){
        return "Salut "+message;
    }
}

package stateful.src;
import javax.ejb.Remote;

@Remote
public interface ReSalut {
    public String salut(String slt);
    public String re_salut();
}

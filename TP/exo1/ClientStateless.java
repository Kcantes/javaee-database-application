import stateless.src.Salut;

import java.util.*;
import javax.naming.*; 

public class ClientStateless {

  public static void main(String[] args) {
    try {
	InitialContext ic = new InitialContext();
        Salut bs = (Salut) ic.lookup("stateless.src.Salut");
        System.out.println(bs.salut("quentin"));
    }
    catch (Exception e) {
       e.printStackTrace();
    }
  }
}
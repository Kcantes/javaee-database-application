import stateful.src.ReSalut;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.naming.*;

class ClientStateful {
    public static void main(String[] args) throws NamingException, FileNotFoundException, IOException {
        InitialContext ic = new InitialContext();
        ReSalut bs = (ReSalut) ic.lookup(ReSalut.class.getName());
        System.out.println(bs.salut("quentin"));
    }
}
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class GestionCarte implements IGestionCarte {
    @PersistenceContext(unitName = "persistenceCarrefour")
    protected EntityManager em;

    public void nouveauCarte(int id, String typeCarte) {
        Carte existant = em.find(Carte.class, id);

        if (existant == null) {
            em.persist(new Carte(id, typeCarte));
        } else {
            System.out.println("Carte deja presente");
        }
    }

    public void suppressionCarte(int id) {
        Carte existant = em.find(Carte.class, id);

        if (existant != null) {
            em.remove(existant);
        } else {
            System.out.println("Carte inexstante");
            ;
        }
    }

    public void attribueCarte(int id) {
        Carte existant = em.find(Carte.class, id);

        if (existant != null) {
            existant.setDispo(0);
            em.persist(existant);
        } else {

        }
    }

    public void resilieCarte(int id) {
        Carte existant = em.find(Carte.class, id);
        if (existant != null) {
            existant.setDispo(1);
            em.persist(existant);
        } else {
        }
    }

    public String printAllCartes() {
        List<Carte> cartes = em.createQuery("SELECT e FROM Carte e", Carte.class).getResultList();
        String s = "\n";
        for (Carte l : cartes) {
            s += l.toString() + "\n";
        }
        return s;
    }

}
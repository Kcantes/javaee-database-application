import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQueries({ @NamedQuery(name = "Carte.listerTous", query = "select e from Carte e"),
        @NamedQuery(name = "Carte.trouverCarteParId", query = "select e from Carte e where e.id = :id")})
@Table(name = "carte")
public class Carte implements Serializable {
    /**
     *
     */
    @Id
    int id;
    String typeCarte;
    int dispo;
    float cagnotte;

    public Carte() {
    }

    public Carte(int id, String typeCarte) {
        this.id = id;
        this.typeCarte = typeCarte;
        this.dispo = 1;
        this.cagnotte = 0;
    }

    public int getId() {
        return id;
    }

    public String getTypeCarte() {
        return typeCarte;
    }

    public void setTypeCarte(String typeCarte) {
        this.typeCarte = typeCarte;
    }

    public void setCagnotte(float cagnotte) {
        this.cagnotte = cagnotte;
    }

    public int getDispo() {
        return this.dispo;
    }

    public void setDispo(int dispo) {
        this.dispo = dispo;
    }

    public String toString() {
        return String.format("Carte (id: %d) - %s - %s - %f€", id, typeCarte,
                this.dispo == 0 ? "possédé" : "disponible", cagnotte);
    }
}

import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ClientCarrefour {
    private IGestionCarte gestionCarte;
    private IGestionFidelite gestionFidelite;
    private IInfosCarte infosCarte;

    public static void main(String[] args) {
        ClientCarrefour client = new ClientCarrefour();
        client.initialisation();

        System.out.println("Test de la création d'une carte");
        client.testAjout();
        client.testLister();
        client.testTrouver();
        System.out.println("Test de la suppression d'une carte");
        client.testSuppression();
        client.testLister();

        System.out.println("Test d'une attribution valide (1, 1)");
        client.testAttribution(1, 1);
        client.testLister();
        System.out.println();

        System.out.println("Test d'une resiliation valide (1, 1)");
        client.testResiliation(1);
        client.testLister();
        System.out.println();

        System.out.println("Test du nombre max d'emprunt atteint");
        client.testAttribution(1, 1);
        client.testAttribution(1, 2);
        client.testAttribution(1, 3);
        client.testLister();

        client.testAttribution(1, 1);

        client.testResiliation(1);
        client.testResiliation(2);
        client.testResiliation(3);
        client.testLister();
        System.out.println();

        System.out.println("Test du conflit d'emprunt");
        client.testAttribution(1, 1);
        client.testAttribution(2, 1);
        client.testResiliation(1);
        client.testLister();
        System.out.println();

    }

    private void initialisation() {
        try {
            InitialContext initialContext = new InitialContext();
            gestionCarte = (IGestionCarte) initialContext.lookup(IGestionCarte.class.getName());
            gestionFidelite = (IGestionFidelite) initialContext.lookup(IGestionFidelite.class.getName());
            infosCarte = (IInfosCarte) initialContext.lookup(IInfosCarte.class.getName());
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    private void testAjout() {
        gestionCarte.nouveauCarte(4, "carte bio");
    }

    private void testLister() {
        infosCarte.listerTous().forEach(l -> {
            System.out.println(l.toString());
        });
    }

    private void testTrouver() {
        System.out.println(infosCarte.trouverCarteParId(1).toString());
    }

    private void testSuppression() {
        gestionCarte.suppressionCarte(4);
    }

    private void testAttribution(int idClient, int id) {
        System.out.printf("> Attribution de la carte %s au client numéro %d%n", id, idClient);
        try {
            gestionFidelite.attribuerCarte(idClient, id);
        } catch (Exception ex) {
            System.out.println("Erreur: " + ex.getMessage());
        }
    }

    private void testResiliation(int id) {
        System.out.printf("> Rendu de la carte %s%n", id);
        try {
            gestionFidelite.resilierCarte(id);
        } catch (Exception e) {
            System.out.println("Erreur: " + e.getMessage());
        }
    }
}
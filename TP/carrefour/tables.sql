create table carte ( id int not null primary key,
typeCarte char(20),
dispo smallint default 1 not null, check (dispo in (0,1)),
cagnotte decimal default 0
);
insert into carte values(1, 'carte basique', 1 ,0);
insert into carte values(2, 'carte gold',1,0);
insert into carte values(3, 'carte basique',1,0);

create table client ( id int not null primary key, nom char(20), email char(20),mdp char(20),nbCartesFidelite int default 0);
create table carte_Client ( id int not null primary key, typeCarte char (20), possesseur int default 0 references client(id), check (possesseur >= 0) );
insert into client values(1, 'Jean','jean@yahoo.fr','jesuisjean',0);
insert into client values(2, 'Pierre','pierre@yahoo.fr','jesuispierre',0);
insert into client values(3, 'Paul','paul@yahoo.fr','jesuispaul',0);

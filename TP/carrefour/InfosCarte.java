import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class InfosCarte implements IInfosCarte{
    @PersistenceContext(unitName = "persistenceCarrefour")
    protected EntityManager em;

    @Override
    public List<Carte> listerTous() {
        TypedQuery<Carte> query = em.createNamedQuery("Carte.listerTous", Carte.class);
        return query.getResultList();
    }

    @Override
    public Carte trouverCarteParId(int id) {
        TypedQuery<Carte> query = em.createNamedQuery("Carte.trouverCarteParId", Carte.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

}

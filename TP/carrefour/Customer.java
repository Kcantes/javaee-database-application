import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="client")
public class Customer {
    @Id
    private int id;
    private String nom;
    private String email;
    private String mdp;
    private int nbCartesFidelite;


    public Customer() {
    }

    public Customer(int id, String nom, String email, String mdp) {
        this.id = id;
        this.nom = nom;
        this.email = email;
        this.mdp = mdp;
        this.nbCartesFidelite=0;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getEmail() {
        return email;
    }

    public int getNbCartesFidelite() {
        return this.nbCartesFidelite;
    }

    public void setNbCartesFidelite(int nbCartesFidelite) {
        this.nbCartesFidelite = nbCartesFidelite;
    }
}

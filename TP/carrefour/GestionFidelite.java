import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class GestionFidelite implements IGestionFidelite {
    private int MAX_CARTE;
    @PersistenceContext(unitName = "persistenceCarrefour")
    protected EntityManager em;

    @Override
    public void attribuerCarte(int idClient, int carteId) throws Exception {
        
        Customer client = em.find(Customer.class, idClient);

        if (client == null) {
            throw new Exception("PasDEmprunteurACeNuméro");
        } else if (client.getNbCartesFidelite() >= MAX_CARTE) {
            throw new Exception("NbMaxCartesAtteint");
        }

        Carte carte = em.find(Carte.class, carteId);

        if (carte == null) {
            throw new Exception("PasDeCarteACeNum");
        }else if(carte.getDispo()==0){
            throw new Exception("CarteDejaAttribuee");
        }

        CarteClient carteClient = new CarteClient(carte.getId(), carte.getTypeCarte(), client.getId());
        em.persist(carteClient);

        carte.setDispo(0);
        em.persist(carte);

        client.setNbCartesFidelite(client.getNbCartesFidelite() + 1);
        em.persist(client);

    }

    @Override
    public void resilierCarte(int id) throws Exception {
        CarteClient carteClient = em.find(CarteClient.class, id);

        if (carteClient == null) {
            throw new Exception("PasDeCarteACeNumero");
        }

        Carte carte = em.find(Carte.class, id);
        carte.setCagnotte(0);
        carte.setDispo(1);
        em.persist(carte);

        Customer client = em.find(Customer.class, carteClient.getPossesseur());
        client.setNbCartesFidelite(client.getNbCartesFidelite() - 1);
        em.persist(client);

        em.remove(carteClient);
    }
    
}

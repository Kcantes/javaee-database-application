import java.util.List;

import javax.ejb.Remote;

@Remote
public interface IInfosCarte{
    public List<Carte> listerTous();
    public Carte trouverCarteParId(int id);
}
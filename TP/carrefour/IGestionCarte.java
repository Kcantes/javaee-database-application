import javax.ejb.Remote;

@Remote
public interface IGestionCarte {
    void nouveauCarte(int id, String typeCarte);
    void suppressionCarte(int id);
    void attribueCarte(int id);
    void resilieCarte(int id);
	public String printAllCartes();
}

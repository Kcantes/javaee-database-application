import javax.ejb.Remote;

@Remote
public interface IGestionFidelite {
    void attribuerCarte(int idClient,int carteId) throws Exception;
    void resilierCarte(int idCarte) throws Exception;
}

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="carte_client")
public class CarteClient {
    @Id private int id;
    private String typeCarte;
    private int possesseur;

    public CarteClient() {
    }

    public CarteClient(int id, String typeCarte, int possesseur) {
        this.id = id;
        this.typeCarte = typeCarte;
        this.possesseur = possesseur;
    }

    public int getId() {
        return id;
    }

    public String getTypeCarte() {
        return typeCarte;
    }

    public int getPossesseur() {
        return possesseur;
    }

}

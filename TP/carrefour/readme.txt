Sujet:
Une carte de fidélité et un client qui peut avoir au max 2 cartes.

Transcription raccord au TP2:
l'emprunteur devient un client, il peut se voir attribuer une carte qui n'est donc plus disponible.
Lorsque le client résilie sa carte, elle est de nouveau disponible.

Le code sql est dans le fichier tables.sql et le jar est deja compilé.

Entités:
Customer
Carte
CarteClient

Beans:
GestionCarte
GestionFidelite
InfosCarte

Interfaces
IGestionCarte
IGestionFidelite
IInfosCarte

Client:
ClientCarrefour
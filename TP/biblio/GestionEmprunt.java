import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class GestionEmprunt implements IGestionEmprunt {
    private int MAX_EMPRUNTS;
    @PersistenceContext(unitName = "persistenceBiblio")
    protected EntityManager em;

    @Override
    public void emprunter(int num_emprunteur, String isbn) throws Exception {
        Emprunteur emprunteur = em.find(Emprunteur.class, num_emprunteur);

        if (emprunteur == null) {
            throw new Exception("PasDEmprunteurACeNuméro");
        } else if (emprunteur.getNblivresemp() >= MAX_EMPRUNTS) {
            throw new Exception("NbMaxEmpruntsAtteint");
        }

        Livre livre = em.find(Livre.class, isbn);

        if (livre == null) {
            throw new Exception("PasDeLivreACeNuméro");
        } else if (livre.getDispo() == 0) {
            throw new Exception("LivreDéjàEmprunté");
        }

        LivreEmp livreEmp = new LivreEmp(livre.getIsbn(), livre.getTitre(), emprunteur.getNumemp());
        em.persist(livreEmp);

        livre.setDispo(0);
        em.persist(livre);

        emprunteur.setNblivresemp(emprunteur.getNblivresemp() + 1);
        em.persist(emprunteur);

    }

    @Override
    public void rendre(String isbn) throws Exception {
        LivreEmp livreEmp = em.find(LivreEmp.class, isbn);

        if (livreEmp == null) {
            throw new Exception("PasDeLivreACeNuméro");
        }

        Livre livre = em.find(Livre.class, isbn);
        livre.setDispo(1);
        em.persist(livre);

        Emprunteur emprunteur = em.find(Emprunteur.class, livreEmp.getEmpruntepar());
        emprunteur.setNblivresemp(emprunteur.getNblivresemp() - 1);
        em.persist(emprunteur);

        em.remove(livreEmp);
    }
    
}

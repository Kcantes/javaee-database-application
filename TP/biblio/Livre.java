import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQueries({
    @NamedQuery(name = "Livre.listerTous", query = "select e from Livre e"),
    @NamedQuery(name = "Livre.trouverLivreParIsbn", query = "select e from Livre e where e.isbn = :isbn")
})
@Table(name="livre")
public class Livre implements Serializable {
    @Id
    String isbn;
    String titre;
    int dispo = 0;

	public Livre() {
    }

    public Livre(String isbn,String titre){
        this.isbn=isbn;
        this.titre = titre;
        this.dispo=1;
    }
	    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getDispo() {
        return dispo;
    }

    public void setDispo(int dispo) {
        this.dispo = dispo;
    }

    public String toString() {
        return String.format("%s (ISBN: %s) - %s", titre, isbn, dispo == 0 ? "emprunté" : "disponible");
    }
}

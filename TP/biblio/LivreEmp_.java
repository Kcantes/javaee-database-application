import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2020-10-21T15:47:41")
@StaticMetamodel(LivreEmp.class)
public class LivreEmp_ { 

    public static volatile SingularAttribute<LivreEmp, String> titre;
    public static volatile SingularAttribute<LivreEmp, String> isbn;
    public static volatile SingularAttribute<LivreEmp, Integer> empruntepar;

}
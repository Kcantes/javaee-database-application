import javax.ejb.Remote;

@Remote
public interface IGestionLivre {
    void nouveauLivre(String isbn, String titre);
    void suppressionLivre(String isbn);
    void emprunteLivre(String isbn);
    void rendreLivre(String isbn);
	public String printAllBooks();
}

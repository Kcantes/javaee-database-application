import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class InfosLivre implements IInfosLivre{
    @PersistenceContext(unitName = "persistenceBiblio")
    protected EntityManager em;

    @Override
    public List<Livre> listerTous() {
        TypedQuery<Livre> query = em.createNamedQuery("Livre.listerTous", Livre.class);
        return query.getResultList();
    }

    @Override
    public Livre trouverLivreParIsbn(String isbn) {
        TypedQuery<Livre> query = em.createNamedQuery("Livre.trouverLivreParIsbn", Livre.class);
        query.setParameter("isbn", isbn);
        return query.getSingleResult();
    }

}

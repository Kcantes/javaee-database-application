import javax.ejb.Remote;

@Remote
public interface IGestionEmprunt {
    void emprunter(int num_emprunteur, String isbn) throws Exception;
    void rendre(String isbn) throws Exception;
}
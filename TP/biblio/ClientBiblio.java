import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ClientBiblio {
    private IGestionLivre gestionLivre;
    private IGestionEmprunt gestionEmprunt;
    private IInfosLivre infosLivre;

    public static void main(String[] args) {
        ClientBiblio client = new ClientBiblio();
        client.initialisation();

        System.out.println("Test de la création d'un livre");
        client.testAjout();
        client.testLister();
        client.testTrouver();
        System.out.println("Test de la suppression d'un livre");
        client.testSuppression();
        client.testLister();

        System.out.println("Test d'un emprunt valide (1, \"333\")");
        client.testEmprunt(1, "333");
        client.testLister();
        System.out.println();

        System.out.println("Test d'un rendu valide (1, \"333\")");
        client.testRendu("333");
        client.testLister();
        System.out.println();

        System.out.println("Test du nombre max d'emprunt atteint");
        client.testEmprunt(1, "111");
        client.testEmprunt(1, "222");
        client.testEmprunt(1, "333");
        client.testLister();

        client.testEmprunt(1, "444");

        client.testRendu("111");
        client.testRendu("222");
        client.testRendu("333");
        client.testLister();
        System.out.println();

        System.out.println("Test du conflit d'emprunt");
        client.testEmprunt(1, "111");
        client.testEmprunt(2, "111");
        client.testRendu("111");
        client.testLister();
        System.out.println();

    }

    private void initialisation() {
        try {
            InitialContext initialContext = new InitialContext();
            gestionLivre = (IGestionLivre) initialContext.lookup(IGestionLivre.class.getName());
            gestionEmprunt = (IGestionEmprunt) initialContext.lookup(IGestionEmprunt.class.getName());
            infosLivre = (IInfosLivre) initialContext.lookup(IInfosLivre.class.getName());
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    private void testAjout() {
        gestionLivre.nouveauLivre("777", "Test livre");
    }

    private void testLister() {
        infosLivre.listerTous().forEach(l -> {
            System.out.println(l.toString());
        });
    }

    private void testTrouver() {
        System.out.println(infosLivre.trouverLivreParIsbn("777").toString());
    }

    private void testSuppression() {
        gestionLivre.suppressionLivre("777");
    }

    private void testEmprunt(int num_emprunteur, String isbn) {
        System.out.printf("> Emprunt du livre %s par l'emprunteur numéro %d%n", isbn, num_emprunteur);
        try {
            gestionEmprunt.emprunter(num_emprunteur, isbn);
        } catch (Exception ex) {
            System.out.println("Erreur: " + ex.getMessage());
        }
    }

    private void testRendu(String isbn) {
        System.out.printf("> Rendu du livre %s%n", isbn);
        try {
            gestionEmprunt.rendre(isbn);
        } catch (Exception e) {
            System.out.println("Erreur: " + e.getMessage());
        }
    }
}
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class GestionLivre implements IGestionLivre {
    @PersistenceContext(unitName = "persistenceBiblio")
    protected EntityManager em;

    public void nouveauLivre(String isbn, String titre) {
        Livre existant = em.find(Livre.class, isbn);
        
        if (existant == null) {
            em.persist(new Livre(isbn, titre));
        } else {
            System.out.println("Livre déjà présent");;
        }
    }

    public void suppressionLivre(String isbn) {
        Livre existant = em.find(Livre.class, isbn);

        if (existant != null) {
            em.remove(existant);
        } else {
            System.out.println("Livre inexstant");;
        }
    }

	public void emprunteLivre(String isbn) {
        Livre existant = em.find(Livre.class, isbn);

        if (existant != null) {
            existant.setDispo(0);
			em.persist(existant);
        } else {

        }
    }
    public void rendreLivre(String isbn) {
        Livre existant = em.find(Livre.class, isbn);

        if (existant != null) {
            existant.setDispo(1);
			em.persist(existant);        
			} else {
			}
    }
    
	public String printAllBooks(){
		List<Livre> livres = em.createQuery("SELECT e FROM Livre e",Livre.class).getResultList();
		String s = "\n";
		for(Livre l : livres){
			s+=l.toString()+"\n";
        }
        return s;
	}

}
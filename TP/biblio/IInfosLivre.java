import java.util.List;

import javax.ejb.Remote;

@Remote
public interface IInfosLivre{
    public List<Livre> listerTous();
    public Livre trouverLivreParIsbn(String isbn);
}